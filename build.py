#!/usr/bin/env python3


from jinja2 import Template, Environment, FileSystemLoader
import yaml
import argparse
import weasyprint

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--template", help="HTML template", default="template/template.html", required=False)
parser.add_argument("-d", "--data", help="Data (yaml) file", default="data.yaml", required=False)
parser.add_argument("-s", "--stylesheet", help="CSS stylesheet", default="template/style.css", required=False)
parser.add_argument("-b", "--basedir", help="Base directory for templates and media", default="./template/", required=False)
parser.add_argument("-o", "--output", help="Output file", default="invoice.pdf", required=False)

args = parser.parse_args()

env = Environment(loader=FileSystemLoader('.'))

template = env.get_template(args.template)

data = None

with open(args.data, "r") as f:
	data = yaml.load(f, Loader=yaml.FullLoader)

meta = data["meta"]

items = data["items"]

total = 0
for item in items:
	total += item["rate"] * item["hours"]

result = template.render(meta=meta, items=items, total=total)

weasyprint.HTML( string=result, base_url=args.basedir ).write_pdf( target=args.output, stylesheets=[ args.stylesheet ] )
